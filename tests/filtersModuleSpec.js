/* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": false}] */
import test from 'tape';
import { fromJS } from 'immutable';
import reduce, {
    loadFiltersSuccess,
    loadFiltersRequest,
    toggleFilter,
} from '../src/modules/filters';

test('loadingRequest action', t => {
    const initialState = fromJS({ isLoading: false });

    const actual = reduce(initialState, loadFiltersRequest()).toJS();

    t.equal(actual.isLoading, true);
    t.end();
});

test('loadFiltersSuccess action', t => {
    const initialState = fromJS({
        all: { year: [] },
        isLoading: true,
    });

    const actual = reduce(initialState, loadFiltersSuccess({ year: [1, 2] })).toJS();

    t.equal(actual.isLoading, false);
    t.deepEqual(actual.all.year, [1, 2]);
    t.end();
});

test('toggleFilter action', t => {
    const initialState = fromJS({ selected: { month: '01' } });

    const actual = reduce(initialState, toggleFilter({ category: 'month', filter: '02' })).toJS();

    t.equal(actual.selected.month, '02');
    t.end();
});


