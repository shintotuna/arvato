/* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": false}] */
import React from 'react';
import { createRenderer } from 'react-addons-test-utils';
import tape from 'tape';
import addAssertions from 'extend-tape';
import jsxEquals from 'tape-jsx-equals';
import Car from '../src/components/Car';

const test = addAssertions(tape, { jsxEquals });

test('React Component Car', t => {
    const props = {
        car: 'abc',
        membership: 20,
        prices: {
            prime: 10,
            regular: 20,
        },
        hours: {
            prime: 2,
            regular: 4,
        },
        total: 100,
    };

    // Test rendered output
    const renderer = createRenderer();
    renderer.render(<Car {...props} />);

    const result = renderer.getRenderOutput();
    t.jsxEquals(result,
        <tr>
            <td>abc</td>
            <td>20€</td>
            <td>10.00€ (2h)</td>
            <td>20.00€ (4h)</td>
            <td>100.00€</td>
        </tr>);

    t.end();
});
