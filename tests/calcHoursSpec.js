/* eslint import/no-extraneous-dependencies: ["error", {"peerDependencies": false}] */
import test from 'tape';
import { splitHours, addStartedMinutes } from '../server/business/calcHours';

test('splits time between prime time and regular hours', t => {
    t.deepEqual(splitHours('06:45', '07:15'), [15, 15]);
    t.deepEqual(splitHours('06:45', '19:15'), [30, 720]);
    t.deepEqual(splitHours('18:45', '19:15'), [15, 15]);
    t.end();
});

test('returns amount of hours based on each 30 minutes started', t => {
    t.equal(addStartedMinutes(0), 0);
    t.equal(addStartedMinutes(31), 1);
    t.equal(addStartedMinutes(1), 0.5);
    t.equal(addStartedMinutes(61), 1.5);
    t.end();
});
