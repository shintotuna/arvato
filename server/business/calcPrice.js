module.exports = (prime, regular, level) => ({
    prime: level === 1 ? prime : prime * 1.5,
    regular: level === 1 ? regular * 0.75 : regular,
});
