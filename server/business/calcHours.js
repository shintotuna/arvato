const newTime = (h, m = 0) => new Date(0, 0, 0, h, m, 0);
const primeTimeStart = newTime(7);
const primeTimeEnd = newTime(19);
const twelveHours = 12 * 60 * 60 * 1000;

const addStartedMinutes = num => Math.ceil(num / 30) / 2;

const splitHours = (start, end) => {
    const timeStart = newTime(...start.split(':'));
    const timeEnd = newTime(...end.split(':'));

    const p1 = Math.max(primeTimeStart - timeStart, 0);
    const p3 = Math.max(timeEnd - primeTimeEnd, 0);
    let p2 = 0;

    if (p1 <= 0) p2 = (p3 <= 0 ? timeEnd : primeTimeEnd) - timeStart;
    else p2 = p3 > 0 ? twelveHours : timeEnd - primeTimeStart;

    return [(p1 + p3) / 60000, p2 / 60000];
};

module.exports = {
    splitHours,
    addStartedMinutes,
    calc: (start, end) => {
        const [regular, prime] = splitHours(start, end).map(addStartedMinutes);

        return { prime, regular };
    },
};
