const calcHours = require('./business/calcHours');
const calcPrice = require('./business/calcPrice');
const calcMembershipPrice = require('./business/calcMembershipPrice');
const validatePrices = require('./business/validatePrices');

const combineResults = results => Object.keys(results).map(month => {
    const houses = Object.keys(results[month]).map(house => {
        const result = {};
        let houseTotal = 0;

        results[month][house].map(item => {
            const { car, hours, prices } = item;
            const membership = calcMembershipPrice(car.level);

            if (!result[car.name]) {
                result[car.name] = {
                    membership,
                    car: car.name,
                    level: car.level,
                    prices: {
                        prime: 0,
                        regular: 0,
                    },
                    hours: {
                        prime: 0,
                        regular: 0,
                    },
                };
            }

            result[car.name].prices.prime += prices.prime;
            result[car.name].prices.regular += prices.regular;
            result[car.name].hours.prime += hours.prime;
            result[car.name].hours.regular += hours.regular;

            return null;
        });

        const cars = Object.keys(result).map(carName => {
            const carData = result[carName];
            const { prime, regular } = carData.prices;
            const carTotal = validatePrices(prime + regular + carData.membership, carData.level);
            houseTotal += carTotal;

            return Object.assign(carData, { total: carTotal });
        });

        return { cars, house, total: houseTotal };
    });

    return { month, houses };
});

module.exports = (data, selectedYear) => {
    const parking = {};

    data.map(item => {
        const { date, house, car: { level } = {}, start, end } = item;
        const [day, month, year] = date.split('/');

        if (year !== selectedYear) return null;

        const hours = calcHours.calc(start, end);
        const prices = calcPrice(hours.prime, hours.regular, level);

        if (!parking[month]) parking[month] = {};
        if (!parking[month][house.name]) parking[month][house.name] = [];

        parking[month][house.name].push(Object.assign({}, item, {
            hours,
            prices,
            date: { day, month, year },
        }));

        return null;
    });

    return {
        year: selectedYear,
        months: combineResults(parking),
    };
};

