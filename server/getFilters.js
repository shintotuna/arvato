module.exports = data => {
    const filters = {
        year: [],
        car: [],
    };

    data.map(({ date, car: { name } }) => {
        const year = date.split('/')[2];

        if (!filters.year.includes(year)) filters.year.push(year);
        if (!filters.car.includes(name)) filters.car.push(name);
    });

    return filters;
};
