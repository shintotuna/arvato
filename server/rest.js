const restify = require('restify');
const data = require('./data.js');
const getFilters = require('./getFilters');
const getInvoices = require('./getInvoices');

const server = restify.createServer();

server.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    return next();
});

server.get('/login', (req, res, next) => {
    res.send({ msg: 'login successful' });
    next();
});

server.get('/invoices/:year', (req, res, next) => {
    res.send(getInvoices(data, req.params.year));
    next();
});

server.get('/filters', (req, res, next) => {
    res.send(getFilters(data));
    next();
});

server.listen(8080, () => console.log('%s listening at %s', server.name, server.url));
