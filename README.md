### Installation
`npm install`
`npm run build`

### Run client
`npm start`

### Run server
`npm run server`

### Run tests
`npm test`

## Technological stack
 
- Client
    - Presentation layer - [react](https://github.com/facebook/react)
    - Data/Store management - [redux](https://github.com/reactjs/redux)
    - Immutable store - [immutable](https://github.com/facebook/immutable-js)
    - Routing - [react-router](https://github.com/ReactTraining/react-router)
- Server - [restify](https://github.com/restify/node-restify)
- Testing - [tape](https://github.com/substack/tape)

