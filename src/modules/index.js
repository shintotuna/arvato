import { combineReducers } from 'redux';
import { createSelector } from 'reselect';
import parkingData, { selectError as filterError, selectEntities } from './parkingData';
import filters, { selectFilters, selectSelectedFilters, selectError as invoiceError } from './filters';

export default combineReducers({
    parkingData,
    filters,
});

// root level selectors
export const getFilters = state => selectFilters(state.filters);
export const getSelectedFilters = state => selectSelectedFilters(state.filters);
export const getInvoices = state => selectEntities(state.parkingData);
export const getSelectedMonth = createSelector(getSelectedFilters, f => f.get('month'));

export const getInvoiceError = state => invoiceError(state.filters);
export const getFilterError = state => filterError(state.parkingData);

export const getFilteredInvoices = year => createSelector(
    getSelectedFilters,
    getInvoices,
    (selectedFilters, invoices) =>
        (invoices.get(String(year)) || []).filter(obj =>
            obj.month === selectedFilters.get('month')
        )
);

