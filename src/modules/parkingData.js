import { createAction, handleActions } from 'redux-actions';
import { fromJS } from 'immutable';
import 'whatwg-fetch';

// initial state
const initial = fromJS({
    entities: {},
    isLoading: false,
    error: null,
});

// constants
const PARKING_DATA_REQUEST = 'PARKING_DATA_REQUEST';
const PARKING_DATA_SUCCESS = 'PARKING_DATA_SUCCESS';
const PARKING_DATA_FAIL = 'PARKING_DATA_FAIL';

// actions
const loadParkingDataRequest = createAction(PARKING_DATA_REQUEST);
const loadParkingDataSuccess = createAction(PARKING_DATA_SUCCESS);
const loadParkingDataFailure = createAction(PARKING_DATA_FAIL);

const fetchParkingData = (year) => fetch(`http://127.0.0.1:8080/invoices/${year}`)
    .then(response => response.json())
    .then(json => json);

export const loadParkingData = (year) => dispatch => {
    dispatch(loadParkingDataRequest());

    return fetchParkingData(year).then(
        data => dispatch(loadParkingDataSuccess(data)),
        error => dispatch(loadParkingDataFailure(error))
    );
};

// selectors

export const selectEntities = state => state.get('entities');
export const selectLoading = state => state.get('isLoading');
export const selectError = state => state.get('error');

// reducer
export default handleActions({
    [loadParkingDataRequest]: state => state.set('isLoading', true),
    [loadParkingDataSuccess]: (state, { payload }) => state
        .setIn(['entities', payload.year], payload.months)
        .set('isLoading', false),
    [loadParkingDataFailure]: state => state
        .set('error', 'Couldn\'t load invoices - no response from server'),

}, initial);
