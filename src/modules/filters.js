import { createAction, handleActions } from 'redux-actions';
import { List, fromJS } from 'immutable';
import 'whatwg-fetch';
import { month } from '../utils/curDate';

// initial state
const initial = fromJS({
    all: {
        year: [],
        month: new Array(12).fill(0).map((v, i) => `0${i + 1}`.slice(-2)),
    },
    isLoading: false,
    selected: { month },
    error: null,
});

// constants
const FILTERS_REQUEST = 'FILTERS_REQUEST';
const FILTERS_SUCCESS = 'FILTERS_SUCCESS';
const FILTERS_FAIL = 'FILTERS_FAIL';
const TOGGLE_FILTER = 'TOGGLE_FILTER';

// actions
export const loadFiltersRequest = createAction(FILTERS_REQUEST);
export const loadFiltersSuccess = createAction(FILTERS_SUCCESS);
export const loadFiltersFailure = createAction(FILTERS_FAIL);

const fetchFilters = () => fetch('http://127.0.0.1:8080/filters')
    .then(response => response.json())
    .then(json => json);

export const loadFilters = () => dispatch => {
    dispatch(loadFiltersRequest());

    return fetchFilters().then(
        data => dispatch(loadFiltersSuccess(data)),
        error => dispatch(loadFiltersFailure(error))
    );
};

export const toggleFilter = createAction(TOGGLE_FILTER);

// selectors
export const selectFilters = state => state.get('all');
export const selectSelectedFilters = state => state.get('selected');
export const selectError = state => state.get('error');

// reducer
export default handleActions({
    [loadFiltersRequest]: state => state.set('isLoading', true),
    [loadFiltersSuccess]: (state, { payload }) => state
        .set('isLoading', false)
        .setIn(['all', 'year'], new List(payload.year)),
    [toggleFilter]: (state, { payload: { category, filter } = {} }) => state
        .setIn(['selected', category], filter),
    [loadFiltersFailure]: state => state
        .set('error', 'Couldn\'t load filters - no response from server'),
}, initial);
