import React, { PropTypes, Component } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { loadParkingData } from '../modules/parkingData';
import { year } from '../utils/curDate';
import {
    getFilteredInvoices,
    getSelectedMonth,
    getInvoiceError,
    getFilterError
} from '../modules/index';
import monthMap from '../utils/monthMap';
import House from './House';

class Invoices extends Component {
    componentDidMount() {
        this.props.loadInvoices(this.props.selectedYear);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.selectedYear !== this.props.selectedYear) {
            this.props.loadInvoices(this.props.selectedYear);
        }
    }

    render() {
        const { invoices, selectedYear, selectedMonth, errors } = this.props;

        return (
            <div className="mui-row">

                <div className="mui-col-sm-10 mui-col-sm-offset-1">
                    <div className="mui--bg-danger">
                        <div className="mui--text-light mui--text-title">
                            {errors.filter}
                        </div>
                        <div className="mui--text-light mui--text-title">
                            {errors.invoice}
                        </div>
                    </div>

                    <br />
                    <br />
                    <div className="mui--text-headline mui--pull-right">
                        {monthMap(selectedMonth)} {selectedYear}
                    </div>
                    <br />

                    {invoices.length > 0 ?
                        invoices.map(({ houses }) =>
                            houses.map((house, key) => <House {...house} key={key} />))
                        : <h4>No invoices for this month</h4>
                    }
                </div>
            </div>
        );
    }
}

Invoices.propTypes = {
    invoices: PropTypes.array.isRequired,
    errors: PropTypes.object,
    selectedYear: PropTypes.string.isRequired,
    selectedMonth: PropTypes.string.isRequired,
    loadInvoices: PropTypes.func.isRequired,
};

export default withRouter(connect(
    (state, routeProps) => {
        const selectedYear = String((routeProps.params && routeProps.params.year) || year);

        return {
            selectedYear,
            selectedMonth: String(getSelectedMonth(state)),
            invoices: getFilteredInvoices(selectedYear)(state),
            errors: {
                invoice: getInvoiceError(state),
                filter: getFilterError(state),
            },
        };
    },
    dispatch => ({ loadInvoices: y => dispatch(loadParkingData(y)) }))(Invoices));
