import React from 'react';
import Filters from './Filters';

const Sidebar = () =>
    <div id="sidebar">
        <h1 className="mui--text-white mui--text-display1 mui--align-vertical">
            Parkify
        </h1>
        <div className="mui-divider" />
        <br />
        <Filters />
    </div>;

export default Sidebar;
