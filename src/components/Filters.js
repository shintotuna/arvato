import React, { PropTypes } from 'react';
import { browserHistory, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { year } from '../utils/curDate';
import { getFilters, getSelectedMonth } from '../modules/index';
import { toggleFilter } from '../modules/filters';
import monthMap from '../utils/monthMap';
import Filter from './Filter';

const Filters = ({ filters, selectedMonth, selectedYear, toggleMonth, toggleYear }) =>
    <div>
        <Filter {...{ toggle: toggleYear, items: filters.year, selected: selectedYear }} />
        <Filter
            {...{
                toggle: toggleMonth,
                items: filters.month,
                selected: monthMap(selectedMonth),
                wrap: monthMap,
            }}
        />
    </div>;

Filters.propTypes = {
    filters: PropTypes.object.isRequired,
    selectedMonth: PropTypes.string.isRequired,
    selectedYear: PropTypes.string.isRequired,
    toggleYear: PropTypes.func.isRequired,
    toggleMonth: PropTypes.func.isRequired,
};

const mapStateToProps = (state, routeProps) => ({
    filters: getFilters(state).toJS(),
    selectedMonth: String(getSelectedMonth(state)),
    selectedYear: String((routeProps.params && routeProps.params.year) || year),
});

const mapActionsToProps = dispatch => ({
    toggleMonth: filter => dispatch(toggleFilter({ category: 'month', filter })),
    toggleYear: f => browserHistory.push(f),
});

export default withRouter(connect(mapStateToProps, mapActionsToProps)(Filters));
