import React, { PropTypes } from 'react';
import format from '../utils/format';

const Car = ({ car, membership, prices, hours, total }) =>
    <tr>
        <td>{car}</td>
        <td>{`${membership}€`}</td>
        <td>{`${format(prices.prime)}€ (${hours.prime}h)`}</td>
        <td>{`${format(prices.regular)}€ (${hours.regular}h)`}</td>
        <td>{`${format(total)}€`}</td>
    </tr>;

Car.propTypes = {
    car: PropTypes.string.isRequired,
    membership: PropTypes.number.isRequired,
    prices: PropTypes.object.isRequired,
    hours: PropTypes.object.isRequired,
    total: PropTypes.number.isRequired,
};

export default Car;
