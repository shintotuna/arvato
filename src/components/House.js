import React, { PropTypes } from 'react';
import format from '../utils/format';
import Car from './Car';

const House = ({ house, cars, total }) =>
    <div>
        <div className="mui--text-accent mui--text-title">{house}</div>
        <br />
        <div className="mui-divider" />
        <table className="mui-table">
            <thead>
                <tr>
                    <th>Car</th>
                    <th>Membership</th>
                    <th>Prime time</th>
                    <th>Regular hours</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody className="mui--divider-bottom">
                {cars.map((car, key) => <Car {...car} key={key} />)}
            </tbody>
            <tfoot>
                <tr>
                    <th />
                    <th />
                    <th />
                    <th className="pull-right">
                        <div className="mui--pull-right">Total:</div>
                    </th>
                    <th>{`${format(total)}€`}</th>
                </tr>
            </tfoot>
        </table>
    </div>;

House.propTypes = {
    house: PropTypes.string.isRequired,
    cars: PropTypes.array.isRequired,
    total: PropTypes.number.isRequired,
};

export default House;
