import React from 'react';
import Invoices from './Invoices';
import Sidebar from './Sidebar';

const Content = () =>
    <div>
        <Sidebar />
        <div id="content" className="mui-container-fluid">
            <Invoices />
        </div>
    </div>;

export default Content;
