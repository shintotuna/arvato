import React, { PropTypes } from 'react';
import Dropdown from 'muicss/lib/react/dropdown';
import DropdownItem from 'muicss/lib/react/dropdown-item';

const Filter = ({ items = [], selected, toggle, wrap = v => v }) =>
    <Dropdown label={selected} alignMenu="right">
        {items.map((val, i) =>
            <DropdownItem onClick={() => toggle(val)} key={i + val}>
                {wrap(val)}
            </DropdownItem>
        )}
    </Dropdown>;

Filter.propTypes = {
    items: PropTypes.array.isRequired,
    selected: PropTypes.string.isRequired,
    toggle: PropTypes.func.isRequired,
    wrap: PropTypes.func,
};

export default Filter;
