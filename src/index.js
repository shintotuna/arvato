import React from 'react';
import { Router, Route, browserHistory } from 'react-router';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Content from './components/Content';
import makeStore from './store';
import { loadFilters } from './modules/filters';

const store = makeStore();

store.dispatch(loadFilters());

ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Route path="/(:year)" component={Content} />
        </Router>
    </Provider>
    , document.getElementById('app'));
