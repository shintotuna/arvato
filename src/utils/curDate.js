export const now = new Date();
export const month = `0${now.getMonth() + 1}`.slice(-2);
export const year = now.getFullYear();
