/* eslint-disable no-param-reassign */

export default (object, cb, returnObj = true) => (
    returnObj ?
        Object.keys(object).reduce((prev, key, i) => {
            prev[key] = cb(object[key], key, i);

            return prev;
        }, {})
        :
        Object.keys(object).reduce((prev, key, i) => {
            prev.push(cb(object[key], key, i));

            return prev;
        }, [])
);

/* eslint-enable no-param-reassign */
